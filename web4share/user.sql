-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2014 at 05:55 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `user`
--
CREATE DATABASE IF NOT EXISTS `user` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `user`;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `img_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `id` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`img_id`, `img_url`, `img_name`, `id`, `extension`) VALUES
(8, 'data/Desert.jpg', 'Desert.jpg', '-1', 'jpg'),
(9, 'data/Hydrangeas.jpg', 'Hydrangeas.jpg', '-1', 'jpg'),
(10, 'data/Jellyfish.jpg', 'Jellyfish.jpg', 'diem', 'jpg'),
(11, 'data/Koala.jpg', 'Koala.jpg', 'admin', 'jpg'),
(12, 'data/book1.png', 'book1.png', 'admin', 'png'),
(13, 'data/chart.png', 'chart.png', 'admin', 'png'),
(15, 'data/chart.png', 'chart.png', 'admin', 'png'),
(16, 'data/home1.png', 'home1.png', 'none', 'png'),
(17, 'data/9+ná»™i dung Ä‘á» tÃ i+danh sÃ¡ch thÃ nh viÃªn.doc', '9+ná»™i dung Ä‘á» tÃ i+danh sÃ¡ch thÃ nh viÃªn.doc', 'admin', 'doc'),
(18, 'data/vong-quang-5-guitar1.jpg', 'vong-quang-5-guitar1.jpg', 'motngay', 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE IF NOT EXISTS `question` (
  `question` varchar(255) NOT NULL,
  `A` varchar(255) NOT NULL,
  `B` varchar(255) NOT NULL,
  `C` varchar(255) NOT NULL,
  `D` varchar(255) NOT NULL,
  `choice` varchar(255) NOT NULL,
  `user` varchar(255) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`question`, `A`, `B`, `C`, `D`, `choice`, `user`, `id`) VALUES
('1+1', '2', '3', '4', '5', 'A', '', 1),
('2+2', '4', '5', '6', '7\r\n', 'A', '', 2),
('3+3', '6', '7', '8', '9', 'A', 'admin', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` char(50) NOT NULL,
  `level` char(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`, `email`) VALUES
(1, 'admin', 'hongphi', '3', 'thesonofdeath@zing.vn'),
(2, 'phidaika', 'hongphi', '1', 'thesonofdeath@zing.vn'),
(4, 'thesonofdeath', 'hongphi', '1', 'thesonofdeath@yahoo.com'),
(5, 'phidaika94', 'phidaika94', '1', 'the@phi.com'),
(6, 'diem', 'diem', '2', 'diemC@gmail.com'),
(10, 'chandoibome', 'hongphi', '1', 'tungtay@yahoo.com'),
(11, 'motngay', 'motngay', '1', 'motngay@motngay.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
